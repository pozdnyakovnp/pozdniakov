package com.example.tinkoff.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.tinkoff.repository.Repository
import com.example.tinkoff.ui.main.DeveloperslifeItem
import kotlinx.coroutines.*

class MainViewModel constructor(private val repository: Repository) : ViewModel() {
    val errorMessage = MutableLiveData<String>()
    var job: Job? = null
    val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        onError("Exception handled: ${throwable.localizedMessage}")
    }
    val loading = MutableLiveData<Boolean>()

    val lastesLiveData = MutableLiveData<MutableList<DeveloperslifeItem>>()
    val topLiveData = MutableLiveData<MutableList<DeveloperslifeItem>>()
    val hotLiveData = MutableLiveData<MutableList<DeveloperslifeItem>>()

    fun getLastes(number: Int) {
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            val response = repository.getLastes(number)
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    lastesLiveData.postValue(response.body()!!.result)
                    loading.value = false
                } else {
                    onError("Error : ${response.message()} ")
                }
            }
        }
    }

    fun getTop(number: Int) {
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            val response = repository.getTop(number)
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    topLiveData.postValue(response.body()!!.result)
                    loading.value = false
                } else {
                    onError("Error : ${response.message()} ")
                }
            }
        }
    }

    fun getHot(number: Int) {
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            val response = repository.getHot(number)
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    hotLiveData.postValue(response.body()!!.result)
                    loading.value = false
                } else {
                    onError("Error : ${response.message()} ")
                }
            }
        }
    }

    private fun onError(message: String) {
        errorMessage.value = message
        loading.value = false
    }

    override fun onCleared() {
        super.onCleared()
        job?.cancel()
    }
}
