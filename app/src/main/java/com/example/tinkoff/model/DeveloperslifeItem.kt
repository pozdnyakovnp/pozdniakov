package com.example.tinkoff.ui.main

class DeveloperslifeRequest {
        var result: ArrayList<DeveloperslifeItem>? = null
        var totalCount = 0
}

data class DeveloperslifeItem(
        var id: Int,
        var description: String?,
        var votes: Int,
        var author: String?,
        var date: String?,
        var gifURL: String?,
        var gifSize: Int,
        var previewURL: String?,
        var videoURL: String?,
        var videoPath: String?,
        var videoSize: Int,
        var type: String?,
        var width: String?,
        var height: String?,
        var commentsCount: Int,
        var fileSize: Int,
        var canVote: Boolean
)