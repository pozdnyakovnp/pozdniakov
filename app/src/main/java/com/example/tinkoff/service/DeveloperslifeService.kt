package com.example.tinkoff.service

import com.example.tinkoff.ui.main.DeveloperslifeRequest
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface DeveloperslifeService {
    @GET("latest/{page}?json=true")
    suspend fun getLatest(@Path("page") page: Int): Response<DeveloperslifeRequest>
    @GET("hot/{page}?json=true")
    suspend fun getHot(@Path("page") page: Int): Response<DeveloperslifeRequest>
    @GET("top/{page}?json=true")
    suspend fun getTop(@Path("page") page: Int): Response<DeveloperslifeRequest>

    companion object {
        var retrofitService: DeveloperslifeService? = null
        fun getInstance() : DeveloperslifeService {
            if (retrofitService == null) {
                val retrofit =
                    Retrofit.Builder()
                        .baseUrl("https://developerslife.ru/")
                        .addConverterFactory(GsonConverterFactory.create())
                        .build()
                        .create(DeveloperslifeService::class.java)
                retrofitService = retrofit
            }
            return retrofitService!!
        }
    }
}