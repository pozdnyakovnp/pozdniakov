package com.example.tinkoff.repository

import com.example.tinkoff.service.DeveloperslifeService


class Repository(private val service: DeveloperslifeService) {
    suspend fun getTop(number: Int) = service.getTop(number)
    suspend fun getHot(number: Int) = service.getHot(number)
    suspend fun getLastes(number: Int) = service.getLatest(number)
}