package com.example.tinkoff.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager2.widget.ViewPager2
import com.example.tinkoff.R
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        var view = inflater.inflate(R.layout.main_fragment, container, false)
        initView(view)
        return view
    }


    private fun initView(view: View) {
        var viewPager = view.findViewById<ViewPager2>(R.id.pager)
        var tabLayout = view.findViewById<TabLayout>(R.id.tab_layout)
        var adapter = TabsFragmentPagerAdapter(this)
        viewPager.adapter = adapter

        TabLayoutMediator(
            tabLayout, viewPager
        ) { tab, position ->
            when (position) {
                0 -> tab.text = getString(R.string.Latest)
                1 -> tab.text = getString(R.string.Best)
                2 -> tab.text = getString(R.string.Hot)
            }
        }.attach()
    }
}