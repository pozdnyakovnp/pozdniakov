package com.example.tinkoff.fragment

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.example.tinkoff.service.DeveloperslifeService
import com.example.tinkoff.viewmodel.MainViewModel
import com.example.tinkoff.viewmodel.ViewModelFactory
import com.example.tinkoff.repository.Repository

class HotFragment : TabsFragment() {
    private val repository = Repository(DeveloperslifeService.getInstance())

    override fun onCreate(savedInstanceState: Bundle?) {
        var vm =
            ViewModelProvider(this, ViewModelFactory(repository)).get(MainViewModel::class.java)
        this.liveData = vm.hotLiveData
        super.onCreate(savedInstanceState)
    }

    override fun getData(number: Int) {
        var vm =
            ViewModelProvider(this, ViewModelFactory(repository)).get(MainViewModel::class.java)
        vm.getHot(number)
    }
}