 package com.example.tinkoff.fragment

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.tinkoff.R
import com.example.tinkoff.ui.main.DeveloperslifeItem

 abstract class TabsFragment : Fragment() {
     lateinit var liveData: MutableLiveData<MutableList<DeveloperslifeItem>>
     private var counter: MutableLiveData<Int> = MutableLiveData()
     private var pageCounter: Int = 1

     private lateinit var imageView: ImageView

     private var items = mutableListOf<DeveloperslifeItem>()
     private var gif: MutableLiveData<String> = MutableLiveData()
     private lateinit var backButton: Button

     override fun onCreate(savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState)

         liveData.observe(this, {
             items.addAll(it)
         })

         counter.postValue(0)
         counter.observe(this, {
             if (it > 1) {
                 backButton.isClickable = true
                 backButton.alpha = 1f
             } else {
                 backButton.isClickable = false
                 backButton.alpha = 0.5f
             }
         })
     }

     abstract fun getData(number: Int)

     override fun onCreateView(
         inflater: LayoutInflater,
         container: ViewGroup?,
         savedInstanceState: Bundle?
     ): View? {
         var rootView = inflater.inflate(R.layout.tabs_fragment, container, false)

         val nextButton = rootView.findViewById<Button>(R.id.nextButton)
         nextButton.setOnClickListener({ onNextClick(it) })
         backButton = rootView.findViewById<Button>(R.id.backButton)
         backButton.setOnClickListener({ onBackClick(it) })

         imageView = rootView.findViewById<ImageView>(R.id.image)

         val circularProgressDrawable = getCircularProgressDrawable()

         gif.observe(viewLifecycleOwner, Observer {
             val options: RequestOptions = RequestOptions()
                 .diskCacheStrategy(DiskCacheStrategy.ALL)
                 .centerCrop()
             Glide.with(this)
                 .load(it)
                 .apply(options)
                 .placeholder(circularProgressDrawable)
                 .error(ColorDrawable(Color.RED))
                 .into(imageView)
         })

         return rootView
     }

     private fun getCircularProgressDrawable(): CircularProgressDrawable {
         val circularProgressDrawable = CircularProgressDrawable(this.requireContext())
         circularProgressDrawable.strokeWidth = 5f
         circularProgressDrawable.centerRadius = 30f
         circularProgressDrawable.start()
         return circularProgressDrawable
     }

     private fun onNextClick(v: View?) {
         if (items.size == counter.value) {
             getData(pageCounter)
             liveData.observe(viewLifecycleOwner, {
                 if (it.size > 0) {
                     gif.value = it.elementAt(0).gifURL
                     counter.value = counter.value!! + 1
                 }
             })
             pageCounter++
         } else {
             gif.value = items[counter.value!!].gifURL
             counter.value = counter.value!! + 1
         }
     }

     private fun onBackClick(v: View?) {
         counter.value = counter.value!! - 1
         gif.value = items[counter.value!!].gifURL
     }
}