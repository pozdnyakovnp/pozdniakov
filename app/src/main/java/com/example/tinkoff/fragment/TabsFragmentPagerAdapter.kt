package com.example.tinkoff.fragment

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter

class TabsFragmentPagerAdapter(mainFragment: MainFragment) :
    FragmentStateAdapter(mainFragment) {

    override fun getItemCount(): Int {
        return ITEM_SIZE
    }

    override fun createFragment(position: Int): Fragment {
        when (position) {
            0 -> return LastesFragment()
            1 -> return TopFragment()
            2 -> return HotFragment()
        }
        throw NotImplementedError()
    }

    companion object {
        private const val ITEM_SIZE = 3
    }
}